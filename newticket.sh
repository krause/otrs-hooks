#!/bin/bash
# YES, we need bash here

# This is a script to send a notification from OTRS to Mattermost.
# Create a new Generic Agent and select "TicketCreate" under event based events.
# Finally, put in the path to the script into the command box.
# Of course you could use it for different notifications, but OTRS is only giving the ticket number and id to the script.
#
# original source: https://gist.github.com/meilon/1ddf647063485e0b64bc

# space seperated please
hook_targets=""
# an actual bash array, because we need to properly handle strings with spaces
# example: protected_queues = ("q1" "q2" "q3 foo bar")
hidden_queues=""


otrs="https://helpdesk.mpib-berlin.mpg.de"
icon="https://mpibox.mpib-berlin.mpg.de/f/15c0639f15f4420097a0/?dl=1"
mattermost="https://chat.mpib-berlin.mpg.de/hooks"
header="Content-Type: application/json"


# we only get ticket id, everything else needs to be picked from the sql db
# for possible values check output of:
#
#   sudo -H -u otrs psql -d otrs2 -A -t -c "select column_name,data_type from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='ticket'"
ticket_id=$2

sql="select t.title,t.customer_user_id,q.name,t.id FROM ticket AS t JOIN queue as q ON (t.queue_id = q.id) WHERE t.id = ${ticket_id}"

# mostly this hook is being called as user "otrs", but once in a while it's
# "www-data" - very encouraging behaviour...
# note, that $USER does not exist, because ENV is slimmed down
# to the bare minimum
if [ "$(whoami)" = "otrs" ] ; then
    # use peer authentication for otrs user
    ticket_data=$(psql -d otrs2 -A -t -c "$sql")
else
    # rely on ~/.pgpass otherwise - configure manually, see /etc/otrs/database.pm
    ticket_data=$(PGPASSFILE=/var/www/.pgpass psql -h localhost -U otrs -d otrs2 -A -t -c "$sql")
fi

# minimal cleanup and defaults, quote at least "
if [ "$ticket_data" != "" ] ; then
    ticket_title=$(echo $ticket_data | rev | cut -d"|" -f4- | rev | sed 's#\"#\\"#g')
    ticket_email=$(echo $ticket_data | rev | cut -d"|" -f3  | rev | sed 's#\"#\\"#g' | cut -d"@" -f1)
    ticket_queue=$(echo $ticket_data | rev | cut -d"|" -f2  | rev | sed 's#\"#\\"#g')
fi
if [ "$ticket_title" = "" ] ; then
    ticket_title="Unkonwn (#${ticket_id})"
fi
if [ "$ticket_email" = "" ] ; then
    ticket_email="Unkonwn"
fi
if [ "$ticket_queue" = "" ] ; then
    ticket_queue="Unkonwn"
fi

# drop markdown chars we use for the message to avoid nesting errors
ticket_queue=$(echo $ticket_queue | tr -d '()[]*')
ticket_title=$(echo $ticket_title | tr -d '()[]*')

# stop if queue is part of data sensitive / or genereally hidden group
for hidden_queue in "${hidden_queues[@]}" ; do
    if [ "$ticket_queue" = "$hidden_queue" ] ; then
        echo "not sending notification (protected/hidden queue)"
        exit 0
    fi
done

request_body=$(< <(cat <<EOF
{
  "username": "OTRSbot",
  "text": "New :ticket: in **$ticket_queue**: [**${ticket_title}**](<$otrs/otrs/index.pl?Action=AgentTicketZoom;TicketID=${ticket_id}>)  from *$ticket_email*",
  "icon_url": "$icon"
}
EOF
))

for hook_target in $hook_targets ; do
    curl -q -i -X POST -H "$header" -d "$request_body" "${mattermost}/${hook_target}" >/dev/null 2>&1
done
exit 0
